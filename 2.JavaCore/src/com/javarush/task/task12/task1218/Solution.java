package com.javarush.task.task12.task1218;

/* 
Есть, летать и двигаться
*/

public class Solution {
    public static void main(String[] args) {

    }

    public interface CanFly {
        public void fly();
    }

    public interface CanMove {
        public void move();
    }

    public interface CanEat {
        public void eat();
    }

    public class Dog implements CanEat, CanMove {
        @Override
        public void move() {
            System.out.println("I am moving");
        }

        @Override
        public void eat() {
            System.out.println("I am eating");
        }
    }

    public class Duck implements CanFly, CanMove, CanEat {
        @Override
        public void fly() {
            System.out.println("I am flying");
        }

        @Override
        public void move() {
            System.out.println("I am moving");
        }

        @Override
        public void eat() {
            System.out.println("I am eating");
        }
    }

    public class Car implements CanMove {
        @Override
        public void move() {
            System.out.println("I am moving");
        }
    }

    public class Airplane implements CanMove, CanFly {
        @Override
        public void fly() {
            System.out.println("I am flying");
        }

        @Override
        public void move() {
            System.out.println("I am moving");
        }
    }
}
