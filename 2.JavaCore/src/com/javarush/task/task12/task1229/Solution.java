package com.javarush.task.task12.task1229;

/* 
Родитель класса CTO
*/

public class Solution {

    public static void main(String[] args) {
        CTO cto = new CTO();
        System.out.println(cto);
    }

    public interface Businessman {
        public void workHard();
    }

    public static class CEO {
        public void workHard() {
            System.out.println("I am working very hard");
        }
    }

    public static class CTO extends CEO implements Businessman {

    }
}
