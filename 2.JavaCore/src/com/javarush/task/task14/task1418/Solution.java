package com.javarush.task.task14.task1418;

import java.util.LinkedList;
import java.util.List;

/* 
Исправь четыре ошибки
*/

public class Solution {

    static List<Number> list = new LinkedList();
    public static List<Number> initList(List<Number> list) {
        list.add(new Double(1000f));
        list.add(new Double("123e-445632"));
        list.add(new Float(-90 / -3));
        list.remove(new Double("123e-445632"));
        return list;
    }
    public static void printListValues(List<Number> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
    public static void processCastedObjects(List<Number> list) {
        for (Number object : list) {
            //Исправь 2 ошибки
            if (object instanceof Float) {
                System.out.println("Is float value defined? " + !(((Float) object).isNaN()));
            } else if (object instanceof Double) {
                System.out.println("Is double value infinite? " + ((Double) object).isInfinite());
            }
            // Исправь 2 ошибки в методе processCastedObjects, связанные с приведением типов:
            //для объекта типа Float нужно вывести "Is float value defined? " + !([Float_object].isNaN()).
            //для объекта типа Double нужно вывести "Is double value infinite? " + [Double_object].isInfinite().
        }
    }
    public static void main(String[] args) {
        printListValues(initList(list));
        processCastedObjects(list);
        //3
        //4 - Исправь 2 ошибки
        //5
    }
}
