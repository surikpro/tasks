package com.javarush.task.task16.task1629;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringJoiner;

/* 
Только по-очереди!
*/

public class Solution {
    public static volatile BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws InterruptedException {
        Read3Strings t1 = new Read3Strings();
        Read3Strings t2 = new Read3Strings();
        t1.start();
        t2.start();
        t1.join();
        t2.join();

        //add your code here - добавьте код тут

        t1.printResult();
        t2.printResult();
    }
    public static class Read3Strings extends Thread {
        private volatile int count = 0;
        StringBuilder stringBuilder = new StringBuilder();
        @Override
        public void run() {
            while (count < 3) {
                try {
                    stringBuilder.append(reader.readLine());
                    stringBuilder.append(" ");
                    count++;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        public void printResult() {
            System.out.println(stringBuilder);
        }
    }

    //add your code here - добавьте код тут
}
