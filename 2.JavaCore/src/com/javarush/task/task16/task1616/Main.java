package com.javarush.task.task16.task1616;

public class Main extends Thread {
    public static void main(String[] args) throws InterruptedException {
        Main main = new Main();
        System.out.println(main.getClass().getName());
        Runnable myThread = new MyThread(1);
        Thread thread1 = new Thread(myThread);

        Runnable secondThread = new MyThread(2);
        Thread thread2 = new Thread(secondThread);
        thread1.start();
        thread1.join();
        thread2.start();

    }
    public static class MyThread implements Runnable {
        private int number;

        public MyThread(int number) {
            this.number = number;
        }

        @Override
        public void run() {
            System.out.println("Thread - " + number);
        }
    }
}
