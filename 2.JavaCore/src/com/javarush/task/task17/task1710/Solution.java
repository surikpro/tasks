package com.javarush.task.task17.task1710;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/* 
CRUD
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws ParseException {
        SimpleDateFormat formatter;
        if (args[0].startsWith("-c")) {
            String strDate = args[3].replace("/", "-");
            formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
            Date birthDate = formatter.parse(strDate);
            if (args[2].equals("м")) {
                Person person = Person.createMale(args[1], birthDate);
                allPeople.add(person);
                System.out.println(allPeople.indexOf(person));
            } else if (args[2].equals("ж")) {
                Person person = Person.createFemale(args[1], birthDate);
                allPeople.add(person);
                System.out.println(allPeople.indexOf(person));
            }

        } else if (args[0].startsWith("-r")) {
            int index = Integer.parseInt(args[1]);
            formatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
            String date = formatter.format(allPeople.get(index).getBirthDate());
            Sex sex = allPeople.get(index).getSex();
            System.out.println(allPeople.get(index).getName() + " " +
                    (sex.equals(Sex.MALE) ? "м" : "ж") + " " + date);
        } else if (args[0].startsWith("-u")) {
            formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            int index = Integer.parseInt(args[1]);
            Person person = allPeople.get(index);
            person.setName(args[2]);
            if (args[3].equals("м")) {
                person.setSex(Sex.MALE);
            } else if (args[3].equals("ж")) {
                person.setSex(Sex.FEMALE);
            }
            person.setBirthDate(formatter.parse(args[4]));
        } else if (args[0].startsWith("-d")) {
            int index = Integer.parseInt(args[1]);
            Person person = allPeople.get(index);
            person.setName(null);
            person.setSex(null);
            person.setBirthDate(null);
        }
    }
}