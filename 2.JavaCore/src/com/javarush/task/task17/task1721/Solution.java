package com.javarush.task.task17.task1721;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/* 
Транзакционность
*/

public class Solution {
    public static List<String> allLines = new ArrayList<String>();
    public static List<String> forRemoveLines = new ArrayList<String>();

    public static void main(String[] args) throws IOException {
//        String filename1 = "C:\\Users\\aydar\\Desktop\\file111.txt";
//        String filename2 = "C:\\Users\\aydar\\Desktop\\file222.txt";
        Scanner scanner = new Scanner(System.in);
        BufferedReader bufferedReader1 = new BufferedReader(new FileReader(scanner.nextLine()));
        while (bufferedReader1.ready()) {
            allLines.add(bufferedReader1.readLine());
        }
        BufferedReader bufferedReader2 = new BufferedReader(new FileReader(scanner.nextLine()));
        while (bufferedReader2.ready()) {
            forRemoveLines.add(bufferedReader2.readLine());
        }
        Solution solution = new Solution();
        solution.joinData();
        bufferedReader1.close();
        bufferedReader2.close();
        scanner.close();
    }

    public void joinData() throws CorruptedDataException {
        if (allLines.containsAll(forRemoveLines)) {
            allLines.removeAll(forRemoveLines);
            System.out.println(allLines.size());
            System.out.println(forRemoveLines.size());
        } else {
            allLines.clear();
            System.out.println(allLines.size());
            throw new CorruptedDataException();
        }
    }
}
