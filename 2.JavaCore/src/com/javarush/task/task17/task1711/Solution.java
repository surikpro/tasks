package com.javarush.task.task17.task1711;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD 2
*/

public class Solution {
    public static volatile List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws ParseException {
        SimpleDateFormat formatter;
        Date birthDate;
        Person person;
        switch (args[0]) {
            case "-c":
                synchronized (allPeople) {
                    int count = 0;
                    for (int i = 1; i < args.length; i += 3) {
                        formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                        birthDate = formatter.parse(args[3 + count]);
                        if (args[2 + count].equals("м")) {
                            person = Person.createMale(args[1 + count], birthDate);
                            allPeople.add(person);
                            System.out.println(allPeople.indexOf(person));
                        } else if (args[2 + count].equals("ж")) {
                            person = Person.createFemale(args[1 + count], birthDate);
                            allPeople.add(person);
                            System.out.println(allPeople.indexOf(person));
                        }
                        count += 3;
                    }
                    break;
                }
            case "-u":
                synchronized (allPeople) {
                    int count = 0;
                    for (int i = 1; i < args.length; i += 4) {
                        formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                        int index = Integer.parseInt(args[1+count]);
                        person = allPeople.get(index);
                        person.setName(args[2+count]);
                        if (args[3+count].equals("м")) {
                            person.setSex(Sex.MALE);
                        } else if (args[3+count].equals("ж")) {
                            person.setSex(Sex.FEMALE);
                        }
                        person.setBirthDate(formatter.parse(args[4+count]));
                        count+=4;
                    }
                    break;
                }
            case "-d":
                synchronized (allPeople) {
                    for (int i = 1; i < args.length; i++) {
                        int index = Integer.parseInt(args[i]);
                        person = allPeople.get(index);
                        person.setName(null);
                        person.setSex(null);
                        person.setBirthDate(null);
                    }
                    break;
                }
            case "-i":
                synchronized (allPeople) {
                    for (int i = 1; i < args.length; i++) {
                        int index = Integer.parseInt(args[i]);
                        formatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
                        String date = formatter.format(allPeople.get(index).getBirthDate());
                        Sex sex = allPeople.get(index).getSex();
                        System.out.println(allPeople.get(index).getName() + " " +
                                (sex.equals(Sex.MALE) ? "м" : "ж") + " " + date);
                    }
                }
                break;
        }
    }
}