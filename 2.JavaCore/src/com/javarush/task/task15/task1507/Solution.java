package com.javarush.task.task15.task1507;

/* 
ООП - Перегрузка
*/

public class Solution {
    public static void main(String[] args) {
        printMatrix(2, 3, "8");
        printMatrix(3, 4, 5445544.5555555);
    }

    public static void printMatrix(int m, int n, String value) {
        System.out.println("Заполняем объектами String");
        printMatrix(m, n, (Object) value);
    }

    public static void printMatrix(int m, int n, Object value) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(value);
            }
            System.out.println();
        }
    }
    public static void printMatrix(int m, int n, Double value) {
        System.out.println("Добавляем в метод Double - " + value);
    }
    public static void printMatrix(int m) {
        System.out.println("Добавляем в метод int - " + m);
    }
    public static void printMatrix(int m, int n) {
        System.out.println("Добавляем в метод int'ы - " + m + ", " + n);
    }
    public static void printMatrix(int m, int n, String s1, String s2) {
        System.out.println("Добавляем в метод Strings - " + s1 + ", " + s2 + ", " + m + ", " + n);
    }
    public static void printMatrix(String s1, String s2) {
        System.out.println("Добавляем в метод Strings - " + s1 + ", " + s2);
    }
    public static void printMatrix(String s1, String s2, String s3) {
        System.out.println("Добавляем в метод Strings - " + s1 + ", " + s2 + ", " + s3);
    }
    public static void printMatrix(String s1, String s2, int a) {
        System.out.println("Добавляем в метод Strings and one int - " + s1 + ", " + s2 + ", " + a);
    }
    public static void printMatrix(String s1, String s2, int a1, int a2) {
        System.out.println("Добавляем в метод Strings - " + s1 + ", " + s2 + ", " + a1 + ", " + a2);
    }
}
