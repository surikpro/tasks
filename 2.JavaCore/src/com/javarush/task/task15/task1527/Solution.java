package com.javarush.task.task15.task1527;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

/* 
Парсер реквестов
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String url = reader.readLine();
        String string = url.split("\\?")[1];
        String[] paramsWithValues = string.split("\\&");
        ArrayList<String> arrayList = new ArrayList<>();
        ArrayList<String> arrayOfValues = new ArrayList<>();
        for (String param : paramsWithValues) {
            String paramName = param.split("\\=")[0];
            arrayList.add(paramName);
            if (paramName.contains("obj")) {
                arrayOfValues.add(param.split("\\=")[1]);;
            }
        }
        for (String param : arrayList) {
            System.out.print(param + " ");
        }
        System.out.println("");
        for (String value : arrayOfValues) {
            try {
                double doubleValue = Double.parseDouble(value);
                alert(doubleValue);
            } catch (NumberFormatException e) {
                alert(value);
            }


        }
        //напишите тут ваш код
    }

    public static void alert(double value) {
        System.out.println("double: " + value);
    }

    public static void alert(String value) {
        System.out.println("String: " + value);
    }
}
