package com.javarush.task.task18.task1809;

import java.io.*;

/* 
Реверс файла
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = bufferedReader.readLine();
        String file2 = bufferedReader.readLine();

        try (FileInputStream fileInputStream = new FileInputStream(file1);
        FileOutputStream fileOutputStream = new FileOutputStream(file2)) {
            byte[] bytes = new byte[fileInputStream.available()];
            while (fileInputStream.available() > 0) {
                fileInputStream.read(bytes);
            }
            for (int i = bytes.length - 1; i >= 0; i--) {
                fileOutputStream.write(bytes[i]);
            }
        }
    }
}
