package com.javarush.task.task18.task1804;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

/* 
Самые редкие байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        HashMap<Integer, Integer> hashMap = new HashMap();

        try (Scanner scanner = new Scanner(System.in);
             InputStream inputStream = new FileInputStream(scanner.nextLine())) {
            int[] bytes = new int[inputStream.available()];
            while (inputStream.available() > 0) {
                for (int i = 0; i < bytes.length; i++) {
                    bytes[i] = inputStream.read();
                }
            }
            int count;
            for (int i = 0; i < bytes.length; i++) {
                if (hashMap.containsKey(bytes[i])) {
                    int counted = hashMap.get(bytes[i]);
                    counted++;
                    hashMap.put(bytes[i], counted);
                } else {
                    count = 1;
                    hashMap.put(bytes[i], count);
                }
            }
            Integer min = Collections.min(hashMap.values());

            for (Map.Entry set : hashMap.entrySet()) {
                if (set.getValue().equals(min)) {
                    System.out.print(set.getKey() + " ");
                }
            }
        }

    }
}
