package com.javarush.task.task18.task1816;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* 
Английские буквы
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(args[0]));
        int count = 0;
        while (bufferedReader.ready()) {
            char letter = (char) bufferedReader.read();
            String string = letter + "";
            Pattern pattern = Pattern.compile("[a-zA-Z]");
            Matcher matcher = pattern.matcher(string);
            if (matcher.matches()) {
                count++;
            }
        }
        System.out.println(count);
        bufferedReader.close();
    }
}
