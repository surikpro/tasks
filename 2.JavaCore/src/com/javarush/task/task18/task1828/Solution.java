package com.javarush.task.task18.task1828;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/* 
Прайсы 2
*/

public class Solution {
    public static class Product {
        int id;
        String name;
        String price;
        String quantity;

        public Product(int id, String name, String price, String quantity) {
            this.id = id;
            this.name = name;
            this.price = price;
            this.quantity = quantity;
        }

        @Override
        public String toString() {
            return String.format("%-8d%-30s%-8s%-4s", id, name, price, quantity);
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            return;
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = bufferedReader.readLine();

        List<Product> products = new ArrayList<>();

        try (BufferedReader fileReader = new BufferedReader(new FileReader(fileName))) {
            while (fileReader.ready()) {
                Product product = getProduct(fileReader.readLine());
                products.add(product);
            }
        }
        switch (args[0]) {
            case "-u":
                Product productToBeUpdated = new Product(Integer.parseInt(args[1].trim()), args[2].trim(), args[3].trim(), args[4].trim());
                try (FileWriter fileWriter = new FileWriter(fileName)) {
                    for (Product product : products) {
                        if (product.id == Integer.parseInt(args[1])) {
                            fileWriter.write(productToBeUpdated.toString());
                        } else {
                            fileWriter.write(product.toString());
                        }
                        fileWriter.write("\n");
                    }
                    break;
                }
            case "-d":
                try (FileWriter fileWriter = new FileWriter(fileName)) {
                    for (Product product: products) {
                        if (product.id != Integer.parseInt(args[1])) {
                            fileWriter.write(product.toString());
                            fileWriter.write("\n");
                        }
                    }
                }
        }
    }

    public static Product getProduct(String string) {
        String id = string.substring(0, 8).trim();
        String name = string.substring(8, 38).trim();
        String price = string.substring(38, 46).trim();
        String quantity = string.substring(46, 50).trim();
        return new Product(Integer.parseInt(id), name, price, quantity);
    }
}
