package com.javarush.task.task18.task1819;

import java.io.*;
import java.util.ArrayList;

/* 
Объединение файлов
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = bufferedReader.readLine();
        String fileName2 = bufferedReader.readLine();

        InputStream inputStream = new FileInputStream(fileName1);
        InputStream inputStream1 = new FileInputStream(fileName2);
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName1));
        ArrayList<Integer> list = new ArrayList<>();
        while (inputStream1.available() > 0) {
            list.add(inputStream1.read());
        }
        while (inputStream.available() > 0) {
            list.add(inputStream.read());
        }

        for (int i = 0; i < list.size(); i++) {
            writer.write(list.get(i));
        }

        writer.close();
        inputStream1.close();
        inputStream.close();
        bufferedReader.close();
    }
}
