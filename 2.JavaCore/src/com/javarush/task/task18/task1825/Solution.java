package com.javarush.task.task18.task1825;

import java.io.*;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

/* 
Собираем файл
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        Set<String> set;
        String fileName = null;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            set = new TreeSet<>();
            while (!(fileName = bufferedReader.readLine()).equals("end")) {
                set.add(fileName);
            }
        }

        String finalFileName = null;
        for (String element : set) {
            if (element.contains(".part")) {
                String[] strings = element.split(".part");
                finalFileName = strings[0];
            }
        }
        try (BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(finalFileName))) {
            for (String element : set) {
                try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(element))) {
                    while (bufferedInputStream.available() > 0) {
                        byte[] bytes = new byte[bufferedInputStream.available()];
                        bufferedInputStream.read(bytes);
                        bufferedOutputStream.write(bytes);
                    }
                }
            }
        }
    }
}

