package com.javarush.task.task18.task1803;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

/* 
Самые частые байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        HashMap<Integer, Integer> hashMap = new HashMap();
        Scanner scanner = new Scanner(System.in);
        InputStream inputStream = new FileInputStream(scanner.nextLine());
        int[] bytes = new int[inputStream.available()];
        while (inputStream.available() > 0) {
            for (int i = 0; i < bytes.length; i++) {
                bytes[i] = inputStream.read();
            }
        }
        int count;
        int max = 1;
        for (int i = 0; i < bytes.length; i++) {
            if (hashMap.containsKey(bytes[i])) {
                int counted = hashMap.get(bytes[i]);
                counted++;
                if (counted > max) {
                    max = counted;
                }
                hashMap.put(bytes[i], counted);
            } else {
                count = 1;
                hashMap.put(bytes[i], count);
            }
        }
        for (Map.Entry set: hashMap.entrySet()) {
            if (set.getValue().equals(max)) {
                System.out.print(set.getKey() + " ");
            }
        }

        inputStream.close();
        scanner.close();
    }
}
