package com.javarush.task.task18.task1817;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* 
Пробелы
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(args[0]));
        int count = 0;
        int spacesCount = 0;
        while (bufferedReader.ready()) {
            char letter = (char) bufferedReader.read();
            if (Character.isWhitespace(letter)) {
                spacesCount++;
            }
            count++;
        }
//        System.out.println(count);
//        System.out.println(spacesCount);
        float result = (float) spacesCount / (float) count * 100.0F;
        System.out.println(String.format("%.2f", result));


        bufferedReader.close();
    }
}
