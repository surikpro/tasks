package com.javarush.task.task18.task1802;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

/* 
Минимальный байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        InputStream inputStream = new FileInputStream(scanner.nextLine());
        int[] bytes = new int[inputStream.available()];
        while (inputStream.available() > 0) {
            for (int i = 0; i < bytes.length; i++) {
                bytes[i] = inputStream.read();
            }
        }
        Arrays.sort(bytes);
        System.out.println(bytes[0]);
        inputStream.close();
        scanner.close();
    }
}
