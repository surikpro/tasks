package com.javarush.task.task18.task1805;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/* 
Сортировка байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = bufferedReader.readLine();
        try (FileInputStream fileInputStream = new FileInputStream(fileName)) {
            Set<Integer> bytes = new HashSet<>();
            while (fileInputStream.available() > 0) {
                for (int i = 0; i < fileInputStream.available(); i++) {
                    bytes.add(fileInputStream.read());
                }
            }
            ArrayList<Integer> list = new ArrayList<>(bytes);
            Collections.sort(list);
            for (Integer element: list) {
                System.out.print(element + " ");
            }
        }
    }
}
