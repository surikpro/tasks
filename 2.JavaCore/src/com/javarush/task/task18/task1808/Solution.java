package com.javarush.task.task18.task1808;

import java.io.*;

/* 
Разделение файла
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = bufferedReader.readLine();
        String fileName2 = bufferedReader.readLine();
        String fileName3 = bufferedReader.readLine();
        try (FileInputStream fileInputStream = new FileInputStream(fileName1);
             FileOutputStream fileOutputStream1 = new FileOutputStream(fileName2);
             FileOutputStream fileOutputStream2 = new FileOutputStream(fileName3)) {
            while (fileInputStream.available() > 0) {
                byte[] bytes = new byte[fileInputStream.available()];
                int count = fileInputStream.read(bytes);
                int half = (count + count%2) / 2;
                    fileOutputStream1.write(bytes, 0, half);
                    fileOutputStream2.write(bytes, half, count - half);
            }
        }

    }
}
