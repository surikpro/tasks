package com.javarush.task.task18.task1818;

import java.io.*;

/* 
Два в одном
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = bufferedReader.readLine();
        String fileName2 = bufferedReader.readLine();
        String fileName3 = bufferedReader.readLine();

        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName1, true));
        BufferedReader bufferedReader1 = new BufferedReader(new FileReader(fileName2));
        BufferedReader bufferedReader2 = new BufferedReader(new FileReader(fileName3));
        while (bufferedReader1.ready()) {
            bufferedWriter.write(bufferedReader1.read());
        }
        while (bufferedReader2.ready()) {
            bufferedWriter.write(bufferedReader2.read());
        }

        bufferedWriter.close();
        bufferedReader.close();
        bufferedReader1.close();
        bufferedReader2.close();
    }
}
