package com.javarush.task.task18.task1822;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Поиск данных внутри файла
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = bufferedReader.readLine();

        BufferedReader bufferedReader1 = new BufferedReader(new FileReader(fileName));
        while (bufferedReader1.ready()) {
            String line = bufferedReader1.readLine();
            String[] data = line.split(" ");
            if (args[0].equals(data[0])) {
                System.out.println(data[0] + " " + data[1] + " " + data[2]+ " " + data[3]);
            }

        }
        bufferedReader1.close();
        bufferedReader.close();
    }
}
