package com.javarush.task.task18.task1801;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

/* 
Максимальный байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        try(Scanner scanner = new Scanner(System.in);
            InputStream inputStream = new FileInputStream(scanner.nextLine());) {
            int[] bytes = new int[inputStream.available()];
            while (inputStream.available() > 0) {
                for (int i = 0; i < bytes.length; i++) {
                    bytes[i] = inputStream.read();
                }
            }
            Arrays.sort(bytes);
            System.out.println(bytes[bytes.length - 1]);
        }
    }
}
