package com.javarush.task.task18.task1823;

import java.io.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* 
Нити и байты
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName;
        while (!(fileName = bufferedReader.readLine()).equals("exit")) {
            ReadThread readThread = new ReadThread(fileName);
            readThread.start();
        }
        bufferedReader.close();
//        for (Map.Entry entry: resultMap.entrySet()) {
//            System.out.println(entry.getKey() + " : " + entry.getValue());
//        }

    }

    public static class ReadThread extends Thread {
        private String fileName;
        public ReadThread(String fileName) {
            this.fileName = fileName;
        }

        @Override
        public void run() {
            Map<Integer, Integer> map = new HashMap<>();
            try (FileInputStream fileInputStream = new FileInputStream(fileName)) {

                while (fileInputStream.available() > 0) {
                    int count = 1;
                    int byteNum = fileInputStream.read();
                    if (map.containsKey(byteNum)) {
                        count = map.get(byteNum);
                        count++;
                        map.put(byteNum, count);
                    } else {
                        map.put(byteNum, count);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
//            for (Map.Entry entry : map.entrySet()) {
//                System.out.println(entry.getKey() + " " + entry.getValue());
//            }
            int maxValue = Collections.max(map.values());
            for (Map.Entry entry : map.entrySet()) {
                if (entry.getValue().equals(maxValue)) {
                    resultMap.put(fileName, (Integer) entry.getKey());
                }
            }
        }



        

        // implement file reading here - реализуйте чтение из файла тут
    }
}
