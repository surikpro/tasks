package com.javarush.task.task13.task1326;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/* 
Сортировка четных чисел из файла
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        // напишите тут ваш код
        InputStream inputStream = null;
        List<String> listArray = new ArrayList();
        List<Integer> newArray;
        try {
            Scanner scanner = new Scanner(System.in);
            inputStream = new FileInputStream(scanner.nextLine());
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            while (reader.ready()) {
                listArray.add(reader.readLine());
            }
            newArray = listArray.stream().map(x -> Integer.parseInt(x)).filter(x -> x % 2 == 0).sorted().collect(Collectors.toList());
            for (int i = 0; i < newArray.size(); i++) {
                System.out.println(newArray.get(i));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            inputStream.close();
        }
    }
}

