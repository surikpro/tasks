package com.javarush.task.task19.task1906;

import java.io.*;

/* 
Четные символы
*/

public class Solution {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileName1 = bufferedReader.readLine();
            String fileName2 = bufferedReader.readLine();
            try (FileReader fileReader = new FileReader(fileName1);
                FileWriter fileWriter = new FileWriter(fileName2)) {
                int counter = 1;
                while (fileReader.ready()) {
                    int number = fileReader.read();
                    if (counter % 2 == 0) {
                        fileWriter.write(number);
                    }
                    counter++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        ;
    }
}
