package com.javarush.task.task19.task1910;

import java.io.*;

/* 
Пунктуация
*/

public class Solution {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileName1 = bufferedReader.readLine();
            String fileName2 = bufferedReader.readLine();
            try (BufferedReader reader = new BufferedReader(new FileReader(fileName1));
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName2))) {
                while (reader.ready()) {
                    String line = reader.readLine().replaceAll("[^a-zA-Zа-яА-Я0-9\\s]", "");
                    writer.write(line);
                }
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
