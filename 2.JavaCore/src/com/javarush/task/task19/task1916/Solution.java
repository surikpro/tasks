package com.javarush.task.task19.task1916;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Отслеживаем изменения
*/

public class Solution {
    public static List<LineItem> lines = new ArrayList<LineItem>();

    public static void main(String[] args) throws IOException {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            List<String> file1 = new ArrayList<>();
            List<String> file2 = new ArrayList<>();
            String fileName1 = bufferedReader.readLine();
            String fileName2 = bufferedReader.readLine();
            try (BufferedReader reader1 = new BufferedReader(new FileReader(fileName1));
                 BufferedReader reader2 = new BufferedReader(new FileReader(fileName2))) {
                while (reader1.ready()) {
                    file1.add(reader1.readLine());
                }
                while (reader2.ready()) {
                    file2.add(reader2.readLine());
                }
//            System.out.println(file1);
//            System.out.println(file2);
                int i = 0;
                while ((i < file1.size() - 1) || (i < file2.size() - 1)) {

                    if (file1.get(i).equals(file2.get(i))) {
                        lines.add(new LineItem(Type.SAME, file1.get(i)));
                    } else if (file1.get(i).equals(file2.get(i + 1)) && !(file2.get(i).equals(file1.get(i + 1)))) {
                        lines.add(new LineItem(Type.ADDED, file2.get(i)));
                        lines.add(new LineItem(Type.SAME, file1.get(i)));
                        file2.remove(i);
                    } else if (file2.get(i).equals(file1.get(i + 1)) && !(file1.get(i).equals(file2.get(i + 1)))) {
                        lines.add(new LineItem(Type.REMOVED, file1.get(i)));
                        lines.add(new LineItem(Type.SAME, file2.get(i)));
                        file1.remove(i);
                    }
                    i++;
                    if ((i == file1.size()) || (i == file2.size())) {
                        if (file1.size() > file2.size()) {
                            lines.add(new LineItem(Type.REMOVED, file1.get(i)));
                        } else {
                            lines.add(new LineItem(Type.ADDED, file2.get(i)));
                        }
                    }
                }
                System.out.println(lines);
            }
        }
    }


    public static enum Type {
        ADDED,        //добавлена новая строка
        REMOVED,      //удалена строка
        SAME          //без изменений
    }

    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }

        @Override
        public String toString() {
            return type + " " + line;
        }
    }
}
