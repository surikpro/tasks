package com.javarush.task.task19.task1915;

import java.io.*;

/* 
Дублируем текст
*/

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            PrintStream consoleStream = System.out;
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            PrintStream stream = new PrintStream(outputStream);
            System.setOut(stream);
            testString.printSomething();
            String result = outputStream.toString();
            System.setOut(consoleStream);
            try (FileOutputStream fileOutputStream = new FileOutputStream(bufferedReader.readLine())) {
                fileOutputStream.write(result.getBytes());
                System.out.println(result);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("it's a text for testing");
        }
    }
}

