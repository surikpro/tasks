package com.javarush.task.task19.task1909;

import java.io.*;
import java.util.ArrayList;

/* 
Замена знаков
*/

public class Solution {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileName1 = bufferedReader.readLine();
            String fileName2 = bufferedReader.readLine();
            try (BufferedReader reader = new BufferedReader(new FileReader(fileName1));
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName2))) {
                while (reader.ready()) {
                    int data = reader.read();
                    if (data == 46) {
                        data = 33;
                    }
                    writer.write(data);

                }
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
