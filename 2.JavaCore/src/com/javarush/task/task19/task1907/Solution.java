package com.javarush.task.task19.task1907;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.stream.Collectors;

/* 
Считаем слово
*/

public class Solution {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            try (FileReader fileReader = new FileReader(bufferedReader.readLine())) {
                StringBuilder stringBuilder = new StringBuilder();
                while (fileReader.ready()) {
                    int data = fileReader.read();
                    stringBuilder.append((char)data);
                }

                String[] splitStrings = stringBuilder.toString().split("\\W");
                System.out.println(Arrays.stream(splitStrings).filter((str) -> str.equals("world")).count());
//                int count = 0;
//                for (String str: splitStrings) {
//                    if (str.equals("world")) {
//                        count++;
//                    }
//                }
//                System.out.println(count);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
