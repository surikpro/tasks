package com.javarush.task.task19.task1914;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

/* 
Решаем пример
*/

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream consoleStream = System.out;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(outputStream);
        System.setOut(stream);
        testString.printSomething();
        String result = outputStream.toString();
        System.setOut(consoleStream);
        String[] str =  result.trim().split(" ");
        int operationResult = 0;
        if (str[1].equals("+")) {
            operationResult = Integer.parseInt(str[0]) + Integer.parseInt(str[2]);
        } else if (str[1].equals("-")) {
            operationResult = Integer.parseInt(str[0]) - Integer.parseInt(str[2]);
        } else if (str[1].equals("*")) {
            operationResult = Integer.parseInt(str[0]) * Integer.parseInt(str[2]);
        }

        System.out.println(str[0] + " " + str[1] + " " + str[2] + " " + str[3] + " " + operationResult);
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("3 + 6 = ");
        }
    }
}

