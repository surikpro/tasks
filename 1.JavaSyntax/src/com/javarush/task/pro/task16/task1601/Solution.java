package com.javarush.task.pro.task16.task1601;

import java.util.*;
import java.util.logging.SimpleFormatter;

/* 
Лишь бы не в понедельник :)
*/

public class Solution {

    static Date birthDate = new Date(85, 5, 12);

    public static void main(String[] args) {
        System.out.println(getDayOfWeek(birthDate));
    }

    static String getDayOfWeek(Date date) {
        //напишите тут ваш код
        List<String> daysInRussian = new ArrayList<>
                (Arrays.asList("Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"));

        return daysInRussian.get(date.getDay());
    }
}

