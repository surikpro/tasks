package com.javarush.task.pro.task16.task1614;

import java.time.Instant;

/* 
Конец времен
*/

public class Solution {

    public static void main(String[] args) {
        System.out.println(getMaxFromMilliseconds());
        System.out.println(getMaxFromSeconds());
        System.out.println(getMaxFromSecondsAndNanos());
    }

    static Instant getMaxFromMilliseconds() {
        //напишите тут ваш код
        Instant instant = Instant.MAX;
        long n = Long.MAX_VALUE;
        Instant time = Instant.ofEpochMilli(n);
        return time;
    }

    static Instant getMaxFromSeconds() {
        //напишите тут ваш код
        Instant instant = Instant.MAX;
        long n = instant.getEpochSecond();
        Instant time = Instant.ofEpochSecond(n);
        return time;
    }

    static Instant getMaxFromSecondsAndNanos() {
        //напишите тут ваш код
        Instant instant = Instant.MAX;
        long n = instant.getEpochSecond();
        long nano = instant.getNano();
        Instant time = Instant.ofEpochSecond(n, nano);
        return time;
    }
}
