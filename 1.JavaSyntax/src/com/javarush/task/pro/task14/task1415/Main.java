package com.javarush.task.pro.task14.task1415;

import java.util.Arrays;
import java.util.Stack;

public class Main {
    public static void main(String[] args) {
        MyStack myStack = new MyStack();
        myStack.push("qw");
        myStack.push("sw");
        myStack.push("aw");
        System.out.println(myStack.peek());
        System.out.println("Deleting: " + myStack.pop());
        System.out.println(myStack.peek());
        System.out.println("Is my stack empty - " + myStack.empty());
        System.out.println(myStack.search("tw"));
    }
//    Stack<Integer> stack = new Stack<>();
}
