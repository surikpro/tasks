package com.javarush.task.pro.task15.task1506;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

/* 
Фейсконтроль
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        Path path = Path.of(scanner.nextLine());
        List<String> lines = Files.readAllLines(path);
        for (String line: lines) {
            for (int i = 0; i < line.length(); i++) {
                if ((line.charAt(i) == '.') || (line.charAt(i) == ',') || (line.charAt(i) == ' ')) {
                    continue;
                }
                System.out.print(line.charAt(i));
            }
        }
    }
}
//          C:\\Users\\aydar\\Desktop\\file1.txt