package com.javarush.task.pro.task15.task1504;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/* 
Перепутанные байты
*/

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        try (InputStream fileOne = Files.newInputStream(Path.of(new Scanner(System.in).nextLine()));
             OutputStream fileTwo = Files.newOutputStream(Path.of(new Scanner(System.in).nextLine()))
        ) {
            byte[] buffer = fileOne.readAllBytes();
            byte[] outputBytes = new byte[buffer.length];
            for (int i = 0; i < buffer.length; i++) {
                if ((i == buffer.length - 1) && (buffer[i] % 2 != 0)) {
                    outputBytes[i] = buffer[i];
                } else if (((i % 2) == 0)) {
                    outputBytes[i + 1] = buffer[i];
                } else {
                    outputBytes[i - 1] = buffer[i];
                }
//            C:\\Users\\aydar\\Desktop\\file1.txt
            }
            fileTwo.write(outputBytes);
        } catch (IOException e) {
            System.out.println("Something went wrong : " + e);
        }
    }
}

