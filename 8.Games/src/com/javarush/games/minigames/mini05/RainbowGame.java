package com.javarush.games.minigames.mini05;

import com.javarush.engine.cell.Game;
import com.javarush.engine.cell.Color;

/* 
Цвета радуги
*/

public class RainbowGame extends Game {
    @Override
    public void initialize() {
        super.initialize();
        setScreenSize(10, 7);
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 7; j++) {
                Color color = null;
                switch (j) {
                    case 0: color = Color.RED;
                        break;
                    case 1: color = Color.ORANGE;
                        break;
                    case 2: color = Color.YELLOW;
                        break;
                    case 3: color = Color.GREEN;
                        break;
                    case 4: color = Color.BLUE;
                        break;
                    case 5: color = Color.INDIGO;
                        break;
                    case 6: color = Color.VIOLET;
                }
                setCellColor(i, j, color);
            }
        }
    }

    @Override
    public void setScreenSize(int width, int height) {
        super.setScreenSize(width, height);
    }

    @Override
    public void setCellColor(int x, int y, Color color) {
        super.setCellColor(x, y, color);
    }
}
